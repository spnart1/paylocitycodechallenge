import React from 'react';
import ReactDOM from "react-dom";
import Redux from "redux";
import FormatCurrency from "format-currency";
import {Button, ButtonToolbar, ButtonGroup, Grid, Row, Col, Well, ControlLabel, Form, Checkbox, FormControl, FieldGroup, HelpBlock, FormGroup, Modal} from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import ToggleDisplay from 'react-toggle-display';
import Axios from "axios";
import {createStore} from 'redux';
import { Provider } from 'react-redux'

//ACTION TYPES;
export const ADD_EMPLOYEE = "ADD_EMPLOYEE";
export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";
export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const SELECT_EMPLOYEE = "SELECT_EMPLOYEE";

export const ADD_DEPENDENT = "ADD_DEPENDENT";
export const DELETE_DEPENDENT = "DELETE_DEPENDENT";
export const UPDATE_DEPENDENT = "UPDATE_DEPENDENT";
export const GET_ALL_EMPLOYEES = "GET_ALL_EMPLOYEES";
export const CLICK_CRUD_BUTTON = "CLICK_CRUD_BUTTON";

//ACTION BUILDERS
function buildAddEmployeeAction(employee){
  return { type : ADD_EMPLOYEE, employee };
}

function buildSelectEmployeeAction(employee){
  return { type : SELECT_EMPLOYEE, employee };
}

function buildDeleteEmployeeAction(employee){
  return { type : DELETE_EMPLOYEE, employee };
}

function buildUpdateEmployeeAction(employee){
  return { type : UPDATE_EMPLOYEE, employee };
}

function buildAddDependentAction(dependent){
  return { type : ADD_DEPENDENT, dependent };
}

function buildDeleteDependentAction(dependent){
  return { type : DELETE_DEPENDENT, dependent };
}

function buildUpdateDependentAction(dependent){
  return { type : UPDATE_DEPENDENT, dependent };
}

function buildGetAllEmployeesAction(data){
  return { type : GET_ALL_EMPLOYEES, data };
}

function buildClickCrudButtonAction(buttonAction){
  return { type: CLICK_CRUD_BUTTON, buttonAction };
}

//ACTION ENACTORS
function addEmployee(state, employee){
  return {
    ...state,
    employees: [...state.employees, employee]
  };
}

function deleteEmployee(state, employee){
  var data;

  return { ...state,
    employees: data.employees,
    payData: data.payData
  };
}

function updateEmployee(state, employee){
  var newState = { ...state,
    employees: state.employees.filter(item => item.id !== employee.id)
  };
  newState.employees.push(employee);
  return newState;
}

function selectEmployee(state, employee){
  var newState = { ...state, selectedEmployee : employee};
  return newState;
}

function getAllEmployees(state, data){
  return { ...state, employees : data.employees, payData : data.payData};
}

function clickCrudButton(state, crudAction){
  if(crudAction.buttonAction == "CloseModal"){
    return { ...state,
      addPanelVisible : false,
      crudAction : crudAction.buttonAction
    }
  }

  if(crudAction.buttonAction == "DeleteEmployee"){
    return { ...state,
      crudAction : crudAction.buttonAction
    }
  }

  return { ...state,
    addPanelVisible : true,
    crudAction : crudAction.buttonAction
  }
}

function addDependent(dependent){
}
function deleteDependent(dependent){
}
function updateDependent(dependent){
}

function actionRelay(state, action = {}){
  switch(action.type){
    case ADD_EMPLOYEE:
      return addEmployee(state, action.employee);
    case DELETE_EMPLOYEE:
      return deleteEmployee(state, action.employee);
    case UPDATE_EMPLOYEE:
      return updateEmployee(state, action.employee);
    case GET_ALL_EMPLOYEES:
      return getAllEmployees(state, action.data);
    case SELECT_EMPLOYEE:
      return selectEmployee(state, action.employee);
    case CLICK_CRUD_BUTTON:
      return clickCrudButton(state, action);
    default:
      return state;
  }
}

function dispatch(appObj, action){
  var newStateObject = actionRelay(state, action);
  appObj.setState(newStateObject);
}

const store = createStore(actionRelay);

var containerDivStyle = {marginTop: "100px"}
let opts = { format: '%s%v', symbol: '$' }

//function FieldEntryGroup({ id, label, help, ...props }) {
class FieldEntryGroup extends React.Component {
  render(){
    return (
      <FormGroup controlId={this.props.id}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl placeholder={this.props.placeholder} />
        {this.props.help && <HelpBlock>{this.props.help}</HelpBlock>}
      </FormGroup>
    );
  }
}

class FieldShowGroup extends React.Component {
  render(){
    return (
      <FormGroup controlId={this.props.id}>
        <ControlLabel>{this.props.label}</ControlLabel>
        <FormControl placeholder={this.props.placeholder} />
      </FormGroup>
    );
  }
}

class TopInfoGridContainer extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      employeeData : {
        employees : [],
        payData : {}
      }
    };
  }

  componentWillMount(){
  }

  render() {
    console.log("TopInfoGridContainer render", this.state, this.props);
    return <TopInfoGrid employees={this.props.employees} payData={this.props.payData} />
  }
}

const TopInfoGrid = (props) => {
  return (
    <Grid>
      <Row className="show-grid">
        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Number of Employees"}</h4>
            {props.employees.length}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Number of Discounts"}</h4>
            {props.payData.grandTotalDiscountCount}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Discounts"}</h4>
            {FormatCurrency(props.payData.grandTotalNetCosts, opts)}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Costs Before Discounts"}</h4>
            {FormatCurrency(props.payData.grandTotalGrossCosts, opts)}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Overall Costs Minus Discounts"}</h4>
            {FormatCurrency(props.payData.grandTotalNetCosts, opts)}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Yearly Gross Pay"}</h4>
            {FormatCurrency(props.payData.grandTotalYearlyGrossPay, opts)}
        </Col>

        <Col xs={6} md={4} lg={3}>
            <h4>{"Total Yearly Net Pay"}</h4>
            {FormatCurrency(props.payData.grandTotalYearlyNetPay, opts)}
        </Col>
      </Row>
    </Grid>
  );
}

TopInfoGridContainer.defaultProps = {
      employeeData : {
        employees : [],
        payData : {}
      }
};

function priceFormatter(cell, row){
  return FormatCurrency(cell, opts);
}

class EmployeeTableContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {}
  }

  render() {
    console.log("EmployeeTableContainer rendered", this.state, this.props);
    return <EmployeeTable performAction={this.props.performAction} employees={this.props.employees} />
  }
}

var selectedEmployeeId;

const EmployeeTable = (props) => {
  const selectRowProp = {
    mode: 'radio',
    onSelect: onRowSelected
  };

  function onRowSelected(row, isSelected){
    props.performAction(buildSelectEmployeeAction(row))
  }

  return (
    <BootstrapTable
      pagination data={props.employees}
      striped={true}
      hover={true}
      selectRow={ selectRowProp }
      >
      <TableHeaderColumn dataField="employeeId" isKey={true} dataAlign="center" dataSort={true}>{"Employee ID"}</TableHeaderColumn>
      <TableHeaderColumn dataField="firstName" dataAlign="center" dataSort={true}>First Name</TableHeaderColumn>
      <TableHeaderColumn dataField="middleName" dataAlign="center">Middle Name</TableHeaderColumn>
      <TableHeaderColumn dataField="lastName" dataAlign="center" dataSort={true}>Last Name</TableHeaderColumn>
      <TableHeaderColumn dataField="ssn" dataSort={true} dataAlign="center">{"SSN"}</TableHeaderColumn>
      <TableHeaderColumn dataField="netBiWeeklyPay" dataSort={true} dataAlign="center" dataFormat={priceFormatter}>{"Net Pay(Bi-Weekly)"}</TableHeaderColumn>
      <TableHeaderColumn dataField="dependentCount" dataSort={true} dataAlign="center">{"# Dependents"}</TableHeaderColumn>
    </BootstrapTable>
  );
}

class AddUpdatePersonModal extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {};

    console.log("CHECK_PROPS", props, this.props);

    if(props.selectedEmployee != null && props.crudAction == "UpdateEmployee"){
      this.state.firstName = props.selectedEmployee.firstName;
      this.state.middleName = props.selectedEmployee.middleName;
      this.state.lastName = props.selectedEmployee.lastName;
      this.state.ssn = props.selectedEmployee.ssn;
    }

    this.getLabel = this.getLabel.bind(this);
    this.onClickClose = this.onClickClose.bind(this);
    this.onClickSave = this.onClickSave.bind(this);
    this.getButtonLabel = this.getButtonLabel.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }

  getLabel(){
    if(this.props.crudAction == "AddEmployee") {
      return "Add Employee";
    }
    else if(this.props.crudAction == "UpdateEmployee") {
      return "Update Employee";
    }
  }

  getButtonLabel(){
    if(this.props.crudAction == "AddEmployee") {
      return "Add";
    }
    else if(this.props.crudAction == "UpdateEmployee") {
      return "Update";
    }
  }

  onClickClose(){
    this.setState({});
    this.props.performAction(buildClickCrudButtonAction("CloseModal"))
  }

  validateForm() {
  }

  onClickSave(){
    var _this = this;
    if(this.props.crudAction == "AddEmployee") {
      Axios.post('api/employee', {
        firstName: _this.state.firstName,
        lastName: _this.state.lastName,
        middleName: _this.state.middleName,
        ssn: _this.state.ssn
      })
      .then(function (response) {
        Axios.get('api/employee').then(function(getResponse) {
          _this.props.performAction(buildGetAllEmployeesAction(getResponse.data));
        });

        _this.setState(emptyState);
        _this.props.performAction(buildClickCrudButtonAction("CloseModal"))
      });
    }
    else if(this.props.crudAction == "UpdateEmployee") {
      Axios.put('api/employee/' + _this.props.selectedEmployee.employeeId, {
        employeeId: _this.props.selectedEmployee.employeeId,
        firstName: _this.state.firstName,
        lastName: _this.state.lastName,
        middleName: _this.state.middleName,
        ssn: _this.state.ssn
      })
      .then(function (response) {
        Axios.get('api/employee').then(function(getResponse) {
          _this.props.performAction(buildGetAllEmployeesAction(getResponse.data));
        });
        _this.setState({employeeId: "",
          ssn: "",
          firstName: "",
          middleName: "",
          lastName: ""});
        _this.props.performAction(buildClickCrudButtonAction("CloseModal"))
      });
    }
  }

  render() {
    return (
      <div className="static-modal">
        <Modal.Dialog>
          <Modal.Header>
            <Modal.Title>{this.getLabel()}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form horizontal>
                <FormGroup controlId="FirstName">
                  <Col componentClass={ControlLabel} sm={4}>
                    {"First Name"}
                  </Col>
                  <Col sm={8}>
                    <FormControl value={this.state.firstName} onChange={(event) =>{ this.setState({firstName : event.target.value})}} type="name" placeholder="First Name" />
                  </Col>
                </FormGroup>

                <FormGroup controlId="MiddleName">
                  <Col componentClass={ControlLabel} sm={4}>
                    {"Middle Name"}
                  </Col>
                  <Col sm={8}>
                    <FormControl  value={this.state.middleName} onChange={(event) =>{ this.setState({middleName : event.target.value})}} type="name" placeholder="Middle Name" />
                  </Col>
                </FormGroup>

                <FormGroup controlId="LastName">
                  <Col componentClass={ControlLabel} sm={4}>
                    {"Last Name"}
                  </Col>
                  <Col sm={8}>
                    <FormControl  value={this.state.lastName} onChange={(event) =>{ this.setState({lastName : event.target.value})}} type="name" placeholder="Last Name" />
                  </Col>
                </FormGroup>

                <FormGroup controlId="SSN">
                  <Col componentClass={ControlLabel} sm={4}>
                    {"SSN"}
                  </Col>
                  <Col sm={8}>
                    <FormControl  value={this.state.ssn} onChange={(event) =>{ this.setState({ssn : event.target.value})}} type="ssn" placeholder="SSN" />
                  </Col>
                </FormGroup>

              </Form>
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={this.onClickClose}>Close</Button>
            <Button onClick={this.onClickSave} bsStyle="primary">{this.getButtonLabel()}</Button>
          </Modal.Footer>

        </Modal.Dialog>
      </div>
    );
  }
}

/*AddUpdatePersonModal.emptyState = {
  employeeId: "",
  ssn: "",
  firstName: "",
  middleName: "",
  lastName: ""
};*/

class CrudButtonBar extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};

    this.onAddClick = this.onAddClick.bind(this);
    this.onUpdateClick = this.onUpdateClick.bind(this);
    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  onAddClick(){
    this.props.performAction(buildClickCrudButtonAction("Add" + this.props.crudTarget));
  }

  onUpdateClick(){
    this.props.performAction(buildClickCrudButtonAction("Update" + this.props.crudTarget))
  }

  onDeleteClick(){
    var _this = this;
    Axios.delete('api/employee/' + _this.props.selectedEmployee.employeeId, {}).then(
      function (response) {
        Axios.get('api/employee').then(function(getResponse) {
          _this.props.performAction(buildGetAllEmployeesAction(getResponse.data));
        });
        _this.props.performAction(buildClickCrudButtonAction("CloseModal"));
      }
    );
    //this.props.performAction(buildClickCrudButtonAction("Delete" + this.props.crudTarget))
  }

  render(){
    return (
      <ButtonToolbar>
        <Button onClick={this.onAddClick}
          bsStyle="primary">Add</Button>
        <Button onClick={this.onUpdateClick} bsStyle="info">Update</Button>
        <Button onClick={this.onDeleteClick} bsStyle="danger">Delete</Button>
      </ButtonToolbar>
    );
  }
}

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
        employees : [],
        payData : {},
        addPanelVisible : false,
        updatePanelVisible : false,
        deletePanelVisible : false
    };

    this.performAction = this.performAction.bind(this);
  }

  componentWillMount(){
    var _this = this;
    Axios.get('api/employee').then(function(response) {
      _this.performAction(buildGetAllEmployeesAction(response.data));
    });
  }

  performAction(action){
    console.log("action", action);
    var newStateObject = actionRelay(this.state, action);
    this.setState(newStateObject);
  }

  render() {
    console.log("app rendered", this.state);
    console.log("selectedEmployee", this.state.selectedEmployee);
    return (
        <div>
          <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container">
              <div className="navbar-header">
                <a className="navbar-brand" href="#">Paylocity Code Challenge</a>
              </div>
            </div>
          </nav>

          <div style={containerDivStyle} className="container">
              <TopInfoGridContainer employees={this.state.employees} payData={this.state.payData}/>
              <CrudButtonBar selectedEmployee={this.state.selectedEmployee} crudTarget={"Employee"} performAction={this.performAction} />
              <EmployeeTableContainer employees={this.state.employees}  performAction={this.performAction} />
              <ToggleDisplay if={this.state.addPanelVisible} tag="section">
                <AddUpdatePersonModal selectedEmployee={this.state.selectedEmployee} crudAction={this.state.crudAction} performAction={this.performAction} />
              </ToggleDisplay>
          </div>
        </div>
    )
  }
}

document.addEventListener("DOMContentLoaded", function() {
  ReactDOM.render(<App/>, document.getElementById("ReactMount"));
});
