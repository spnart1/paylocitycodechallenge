const path = require('path');

var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

console.log(__dirname);

module.exports = {
  entry: [
    './src/app.jsx',
  ],
  output: {
    path: path.join(__dirname, '/../PaylocityCodeChallenge/Content'),
    filename: 'bundle.js',
  },
  node: {
    fs: 'empty',
    tls: 'empty',
    dgram: 'empty'
},
  module: {
    loaders: [{
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ["latest", "stage-2", "react", "es2015"]
        }
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!autoprefixer-loader'
        })
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!autoprefixer-loader!sass-loader'
        })
      },
      {
        test: /\.(jpg|png)$/,
        loader: 'url-loader'
      }
    ]
  },
  watch: false,
  plugins: [
    new ExtractTextPlugin("bundle.css")
  ],
  resolve: {
    alias: {
      'osc-js': path.join(__dirname,'./node_modules/osc-js/lib/osc.browser.js'),
    },
    modules: [
      path.join(__dirname, 'node_modules'),
    ],
  }
};
