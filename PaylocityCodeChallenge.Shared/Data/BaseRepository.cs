﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data
{
    public class BaseRepository
    {
        protected IDbTransaction Transaction { get; private set; }

        protected IDbConnection Connection
        {
            set { }
            get
            {
                return Transaction.Connection;
            }
        }

        public void setTransaction(IDbTransaction transaction)
        {
            Transaction = transaction;
        }

        public AddressRepository GetAddressRepository()
        {
            AddressRepository addressRepository = new AddressRepository();
            addressRepository.setTransaction(this.Transaction);
            return addressRepository;
        }

        public DependentRepository GetDependentRepository()
        {
            DependentRepository dependentRepository = new DependentRepository();
            dependentRepository.setTransaction(this.Transaction);
            return dependentRepository;
        }

        public EmployeeRepository GetEmployeeRepository()
        {
            EmployeeRepository employeeRepository = new EmployeeRepository();
            employeeRepository.setTransaction(this.Transaction);
            return employeeRepository;
        }
    }
}