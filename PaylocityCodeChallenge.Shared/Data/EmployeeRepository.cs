﻿using Dapper;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace PaylocityCodeChallenge.Shared.Data
{
    public class EmployeeRepository : BaseRepository, IEmployeeRepository
    {
        public Employee Get(int employeeId)
        {
            var queryParams = new { EmployeeId = employeeId };

            string query = @"
                SELECT 
	                e.Id as EmployeeId,
	                --person 
	                ep.Id as PersonId,
	                ep.SSN, 
	                ep.FirstName, 
	                ep.MiddleName, 
	                ep.LastName, 
	                ep.AddressId,
	                --address
	                a.Id as AddressId, 
	                a.Street1, 
	                a.Street2, 
	                a.City, 
	                a.[State], 
	                a.Zip,
	                --dependents
	                d.Id as DependentId,
	                dp.Id as PersonId,
	                dp.SSN, 
	                dp.FirstName, 
	                dp.MiddleName, 
	                dp.LastName,
	                dp.AddressId,
	                --dependents address
	                'ignore' as DependentAddress,
	                da.Id as AddressId, 
	                da.Street1, 
	                da.Street2, 
	                da.City, 
	                da.[State], 
	                da.Zip
                FROM [Employee] e
                JOIN [Person] ep ON  e.PersonId = ep.Id
                LEFT JOIN [Address] a ON a.Id = ep.AddressId
                LEFT JOIN [Dependent] d ON d.EmployeeId = e.Id
                LEFT JOIN [Person] dp ON d.PersonId = dp.Id
                LEFT JOIN [Address] da ON da.Id = dp.AddressId
                WHERE e.Id = @EmployeeId";

            Dictionary<int, Employee> employeeDictionary = new Dictionary<int, Employee>();

            Employee employeeOut = Connection.Query<Employee, Address, Dependent, Address, Employee>(
                sql: query,
                map: (emp, add, dep, depAdd) =>
                {
                    //var employeeDictionary = new Dictionary
                    Employee employeeEntry;

                    if (!employeeDictionary.TryGetValue(emp.EmployeeId, out employeeEntry))
                    {
                        employeeEntry = emp;
                        employeeEntry.Dependents = new List<Dependent>();
                        employeeEntry.Address = add;
                        employeeDictionary.Add(employeeEntry.EmployeeId, employeeEntry);
                    }

                    if (dep != null && dep.DependentId > 0)
                    {
                        if (depAdd != null)
                        {
                            dep.Address = depAdd;
                        }
                        employeeEntry.Dependents.Add(dep);
                    }

                    return employeeEntry;
                },
                param: queryParams,
                splitOn: "AddressId,DependentId,DependentAddress",
                transaction: Transaction).FirstOrDefault();

            if(employeeOut != null && employeeOut.Dependents.Count == 0)
            {
                employeeOut.Dependents = null;
            }

            return employeeOut;
        }

        public IEnumerable<Employee> GetAll()
        {
            string query = @"
                SELECT 
	                e.Id as EmployeeId,
	                --person 
	                ep.Id as PersonId,
	                ep.SSN, 
	                ep.FirstName, 
	                ep.MiddleName, 
	                ep.LastName, 
	                ep.AddressId,
	                --address
	                a.Id as AddressId, 
	                a.Street1, 
	                a.Street2, 
	                a.City, 
	                a.[State], 
	                a.Zip,
	                --dependents
	                d.Id as DependentId,
	                dp.Id as PersonId,
	                dp.SSN, 
	                dp.FirstName, 
	                dp.MiddleName, 
	                dp.LastName,
	                dp.AddressId,
	                --dependents address
	                'ignore' as DependentAddress,
	                da.Id as AddressId, 
	                da.Street1, 
	                da.Street2, 
	                da.City, 
	                da.[State], 
	                da.Zip
                FROM [Employee] e
                JOIN [Person] ep ON  e.PersonId = ep.Id
                LEFT JOIN [Address] a ON a.Id = ep.AddressId
                LEFT JOIN [Dependent] d ON d.EmployeeId = e.Id
                LEFT JOIN [Person] dp ON d.PersonId = dp.Id
                LEFT JOIN [Address] da ON da.Id = dp.AddressId";

            Dictionary<int, Employee> employeeDictionary = new Dictionary<int, Employee>();

            IEnumerable<Employee> employeeOut = Connection.Query<Employee, Address, Dependent, Address, Employee>(
                sql: query,
                map: (emp, add, dep, depAdd) =>
                {
                    //var employeeDictionary = new Dictionary
                    Employee employeeEntry;

                    if (!employeeDictionary.TryGetValue(emp.EmployeeId, out employeeEntry))
                    {
                        employeeEntry = emp;
                        employeeEntry.Dependents = new List<Dependent>();
                        employeeEntry.Address = add;
                        employeeDictionary.Add(employeeEntry.EmployeeId, employeeEntry);
                    }

                    if (dep != null && dep.DependentId > 0)
                    {
                        if (depAdd != null)
                        {
                            dep.Address = depAdd;
                        }
                        employeeEntry.Dependents.Add(dep);
                    }

                    return employeeEntry;
                },
                splitOn: "AddressId,DependentId,DependentAddress",
                transaction: Transaction);

            return employeeDictionary.Values;
        }

        /*public IEnumerable<Employee> GetAll(bool? includeDependentCount)
        {
            string query;

            if (includeDependentCount != null && includeDependentCount == true)
            {
                query = @"
                    SELECT e.Id as EmployeeId, p.Id as PersonId, p.SSN, p.FirstName, p.MiddleName, p.LastName, COUNT(d.Id) as DependentCount
                    FROM[Employee] e
                    JOIN[Person] p ON  e.PersonId = p.Id
                    LEFT JOIN[Dependent] d ON d.EmployeeId = e.Id
                    GROUP BY e.Id, p.Id, p.SSN, p.FirstName, p.MiddleName, p.LastName
                ";
            }
            else
            {
                query = @"
                    SELECT e.Id as EmployeeId, p.Id as PersonId, p.SSN, p.FirstName, p.MiddleName, p.LastName
                    FROM [Employee] e
                    JOIN [Person] p ON  e.PersonId = p.Id
                ";
            }

            var employeesOut = Connection.Query<Employee>(sql: query, transaction: Transaction).ToList();

            return employeesOut;
        }*/

        public Employee Add(Employee employee)
        {
            string query = @"
                DECLARE @Id INT;
		        INSERT INTO [Person] (SSN, FirstName, MiddleName, LastName)
		        VALUES (@SSN, @FirstName, @MiddleName, @LastName)

		        SET @Id = SCOPE_IDENTITY()

		        INSERT INTO [Employee] (PersonId)
		        VALUES (@Id)

                SET @Id = SCOPE_IDENTITY()

                SELECT TOP 1 e.Id as EmployeeId
                FROM [Employee] e
                JOIN [Person] p ON  e.PersonId = p.Id
                WHERE e.Id = @Id";

            try
            {
                Employee employeeOut = Connection.Query<Employee>(
                sql: query,
                param: employee,
                transaction: Transaction).FirstOrDefault();

                if (employee.Dependents != null)
                {
                    foreach (Dependent dependent in employee.Dependents)
                    {
                        this.GetDependentRepository().Add(employeeOut.EmployeeId, dependent);
                    }
                }
                employeeOut = this.Get(employeeOut.EmployeeId);

                return employeeOut;
            }
            catch (SqlException)
            {
                Transaction.Rollback();
                throw;
            }
        }

        public Employee Update(int employeeId, Employee employee)
        {
            if (employeeId != 0)
            {
                employee.EmployeeId = employeeId;
            }

            string query = @"
                DECLARE @EmployeePersonId INT;

                SELECT TOP 1 @EmployeePersonId = PersonId
                FROM Employee
                WHERE [Id] = @EmployeeId

                IF @EmployeePersonId IS NOT NULL
                BEGIN
	                UPDATE [Person]
	                SET FirstName = @FirstName,
	                MiddleName = @MiddleName,
	                LastName = @LastName,
	                SSN = @SSN
	                WHERE Id = @EmployeePersonId	

	                SELECT 
	                    e.Id as EmployeeId,
	                    e.PersonId,
	                    p.SSN,
	                    p.FirstName,
	                    p.MiddleName,
	                    p.LastName,
	                    p.AddressId
                    FROM Employee e
                    JOIN Person p ON p.Id = e.PersonId
                    WHERE e.Id = @EmployeeId
                END";

            bool rolledBack = false;

            try
            {
                Employee employeeOut = Connection.Query<Employee>(
                    sql: query,
                    param: employee,
                    transaction: Transaction).FirstOrDefault();

                if (employeeOut != null)
                {
                    try
                    {
                        if (employeeOut.AddressId > 0 && employee.Address != null)
                        {
                            employeeOut.Address = this.GetAddressRepository().Update(employeeOut.AddressId, employee.Address);
                        }
                        else if (employeeOut.AddressId == 0 && employee.Address != null)
                        {
                            employeeOut.Address = this.GetAddressRepository().Add(employeeOut.PersonId, employee.Address);
                        }
                    }
                    catch (SqlException ex)
                    {
                        rolledBack = true;
                        throw;
                    }
                }

                return Get(employeeId);
            }
            catch (SqlException)
            {
                if (!rolledBack)
                {
                    Transaction.Rollback();
                }
                throw;
            }
        }

        public bool Delete(int employeeId)
        {
            string query = @"
                DECLARE @EmployeePersonId INT
                DECLARE @EmployeeAddressId INT

                SELECT TOP 1 
	                @EmployeePersonId = e.PersonId,
	                @EmployeeAddressId = p.AddressId
                FROM Employee e
                JOIN [Person] p ON p.Id = e.PersonId
                WHERE e.Id = @EmployeeId

                IF @EmployeePersonId IS NOT NULL
                BEGIN
	                DELETE FROM Employee
	                WHERE Id = @EmployeeId

	                IF NOT EXISTS(
		                SELECT 1 FROM Dependent
		                WHERE PersonId = @EmployeePersonId
	                )
	                BEGIN
		                DELETE FROM [Person]
		                WHERE Id = @EmployeePersonId
	                END

	                DELETE FROM [Address]
	                WHERE Id = @EmployeeAddressId
                END";

            bool rolledBack = false;

            try
            {
                Employee queriedEmployee = this.Get(employeeId);

                if (queriedEmployee != null)
                {
                    var dependentRepository = this.GetDependentRepository();

                    if (queriedEmployee.Dependents != null) {
                        foreach (var dependent in queriedEmployee.Dependents)
                        {
                            dependentRepository.Delete(dependent.DependentId);
                        }
                    }
                    try
                    {
                        Connection.Query(
                        sql: query,
                        param: new { EmployeeId = employeeId },
                        transaction: Transaction);
                    }
                    catch (SqlException ex)
                    {
                        rolledBack = true;
                        throw;
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException)
            {
                if (!rolledBack)
                {
                    Transaction.Rollback();
                }
                throw;
            }
        }
    }
}
