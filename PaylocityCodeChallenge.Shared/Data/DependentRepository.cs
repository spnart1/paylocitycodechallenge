﻿using Dapper;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data
{
    public class DependentRepository : BaseRepository, IDependentRepository
    {
        public Dependent Get(int dependentId)
        {
            string query = @"
                SELECT 
	                d.Id as DependentId,
	                d.PersonId,
	                p.SSN,
	                p.FirstName,
	                p.MiddleName,
	                p.LastName,
	                p.AddressId
                FROM Dependent d
                JOIN Person p ON p.Id = d.PersonId
                WHERE d.Id = @DependentId";

            Dependent dependentOut = Connection.Query<Dependent>(
                sql: query,
                param: new { DependentId = dependentId },
                transaction: Transaction).FirstOrDefault();

            return dependentOut;
        }

        public Dependent Add(int employeeId, Dependent dependent)
        {
            var queryParams = new
            {
                EmployeeId = employeeId,
                dependent.DependentId,
                dependent.SSN,
                dependent.FirstName,
                dependent.MiddleName,
                dependent.LastName
            };

            string query = @"
                DECLARE @InsertId INT;

                IF EXISTS(
	                SELECT TOP 1 1
	                FROM Employee
	                WHERE Id = @EmployeeId
                )
                BEGIN
	                IF EXISTS(
		                SELECT TOP 1 1
		                FROM Dependent
		                WHERE Id = @DependentId
	                )
	                BEGIN
		                UPDATE Dependent
		                SET EmployeeId = @EmployeeId
		                WHERE Id = @DependentId
	                END
	                ELSE
	                BEGIN
		                INSERT INTO [Person] (SSN, FirstName, MiddleName, LastName)
		                VALUES (@SSN, @FirstName, @MiddleName, @LastName)

		                SET @InsertId = SCOPE_IDENTITY()

		                INSERT INTO Dependent(PersonId, EmployeeId)
		                VALUES (@InsertId, @EmployeeId)

		                SET @InsertId = SCOPE_IDENTITY()

		                SELECT d.Id as DependentId,
			                d.EmployeeId as EmployeeId,
			                p.Id as PersonId, p.SSN, 
			                p.FirstName, 
			                p.MiddleName, 
			                p.LastName, 
			                p.AddressId
		                FROM [Dependent] d
		                JOIN [Person] p ON  d.PersonId = p.Id
		                WHERE d.Id = @InsertId
	                END
                END";

            bool rolledBack = false;

            try
            {
                Dependent dependentOut = Connection.Query<Dependent>(
                    sql: query,
                    param: queryParams,
                    transaction: Transaction).FirstOrDefault();

                if(dependentOut != null && dependentOut.AddressId == 0 && dependent.Address != null)
                {
                    try
                    {
                        Address addressOut = this.GetAddressRepository().Add(dependentOut.PersonId, dependent.Address);
                        dependentOut.Address = addressOut;
                    }
                    catch (SqlException ex)
                    {

                        //there was a rollback, we don't want double rollbacks, so skip the next
                        //probably a cleaner way to handle this
                        rolledBack = true;
                        throw;
                    }
                }

                return dependentOut;
            }
            catch (SqlException ex)
            {
                if (!rolledBack)
                {
                    Transaction.Rollback();
                }
                throw;
            }
        }

        public Dependent Update(int dependentId, Dependent dependent)
        {
            if (dependentId != 0)
            {
                dependent.DependentId = dependentId;
            }

            string query = @"
                DECLARE @DependentPersonId INT;

                SELECT TOP 1 @DependentPersonId = PersonId
                FROM [Dependent]
                WHERE [Id] = @DependentId

                IF @DependentPersonId IS NOT NULL
                BEGIN
	                UPDATE [Person]
	                SET FirstName = @FirstName,
	                MiddleName = @MiddleName,
	                LastName = @LastName,
	                SSN = @SSN
	                WHERE Id = @DependentPersonId	

	                SELECT 
	                    d.Id as DependentId,
	                    d.PersonId,
	                    p.SSN,
	                    p.FirstName,
	                    p.MiddleName,
	                    p.LastName,
	                    p.AddressId
                    FROM Dependent d
                    JOIN Person p ON p.Id = d.PersonId
                    WHERE d.Id = @DependentId
                END";

            bool rolledBack = false;

            try
            {
                Dependent dependentOut = Connection.Query<Dependent>(
                    sql: query,
                    param: dependent,
                    transaction: Transaction).FirstOrDefault();

                if (dependentOut != null)
                {
                    try
                    {
                        if (dependentOut.AddressId > 0 && dependent.Address != null)
                        {
                            dependentOut.Address = this.GetAddressRepository().Update(dependentOut.AddressId, dependent.Address);
                        }
                        else if (dependentOut.AddressId == 0 && dependent.Address != null)
                        {
                            dependentOut.Address = this.GetAddressRepository().Add(dependentOut.PersonId, dependent.Address);
                        }
                    }
                    catch (SqlException ex)
                    {
                        rolledBack = true;
                        throw;
                    }
                }

                return dependentOut;
            }
            catch (SqlException)
            {
                if (!rolledBack)
                {
                    Transaction.Rollback();
                }
                throw;
            }
        }

        public void Delete(int dependentId)
        {
            string query = @"
	        DECLARE @DependentPersonId INT
            DECLARE @DependentAddressId INT

            SELECT TOP 1 
	            @DependentPersonId = d.PersonId,
	            @DependentAddressId = p.AddressId
            FROM [Dependent] d
            JOIN [Person] p ON p.Id = d.PersonId
            WHERE d.Id = @DependentId

            IF @DependentPersonId IS NOT NULL
            BEGIN
	            DELETE FROM [Dependent]
	            WHERE Id = @DependentId

	            IF NOT EXISTS(
		            SELECT 1 FROM Employee
		            WHERE PersonId = @DependentPersonId
	            )
	            BEGIN
		            DELETE FROM [Person]
		            WHERE Id = @DependentPersonId
	            END

	            DELETE FROM [Address]
	            WHERE Id = @DependentAddressId
            END";

            try
            {
                Connection.Query(
                    sql: query,
                    param: new { DependentId = dependentId },
                    transaction: Transaction);
            }
            catch (SqlException)
            {
                Transaction.Rollback();
                throw;
            }
        }
    }
}