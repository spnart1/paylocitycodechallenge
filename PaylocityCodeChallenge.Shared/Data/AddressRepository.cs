﻿using Dapper;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data
{
    public class AddressRepository : BaseRepository, IAddressRepository
    {
        public Address Get(int addressId)
        {
            string query = @"
                SELECT 
	               a.Id as AddressId,
	                a.Street1,
	                a.Street2,
	                a.City,
	                a.Zip
                FROM [Address] a
                JOIN [Person] p ON p.AddressId = a.Id
                WHERE a.Id = @AddressId";

            Address addressOut = Connection.Query<Address>(
                sql: query,
                param: new { AddressId = addressId },
                transaction: Transaction).FirstOrDefault();

            return addressOut;
        }

        public Address Add(int personId, Address address)
        {
            var queryParams = new
            {
                PersonId = personId,
                address.Street1,
                address.Street2,
                address.City,
                address.State,
                address.Zip
            };

            string query = @"
                DECLARE @AddressId INT;

		        SELECT @AddressId = [Address].Id
		        FROM [Address]
		        JOIN [Person] ON AddressId = Address.Id
		        WHERE [Person].Id = @PersonId

		        IF @AddressId IS NULL
		        BEGIN
			        INSERT INTO [Address] (Street1, Street2, City, [State], Zip)
			        VALUES (@Street1, @Street2, @City, @State, @Zip)
	
                    SET @AddressId = SCOPE_IDENTITY()

			        UPDATE [Person]
			        SET [AddressId] = SCOPE_IDENTITY()
			        WHERE [Person].Id = @PersonId
		        END

                SELECT 
	                a.Id as AddressId,
	                a.Street1,
	                a.Street2,
	                a.City,
	                a.Zip
                FROM [Address] a
                WHERE a.Id = @AddressId";

            try
            {
                Address addressOut = Connection.Query<Address>(
                    sql: query,
                    param: queryParams,
                    transaction: Transaction).FirstOrDefault();

                return addressOut;
            }
            catch (SqlException ex)
            {
                Transaction.Rollback();
                throw;
            }
        }

        public Address Update(int addressId, Address address)
        {
            if(addressId != 0)
            {
                address.AddressId = addressId;
            }

            string query = @"
                IF EXISTS(
	                SELECT TOP 1 1
	                FROM [Address]
	                WHERE [Id] = @AddressId
                )
                BEGIN
	                UPDATE [Address]
	                   SET [Street1] = @Street1,
		                  [Street2] = @Street2,
		                  [City] = @City,
		                  [State] = @State,
		                  [Zip] = @Zip
	                 WHERE [Id] = @AddressId

	                SELECT TOP 1 
		                [Id] as AddressId,
		                [Street1],
		                [Street2],
		                [City],
		                [State],
		                [Zip]
	                FROM [Address]
	                WHERE [Id] = @AddressId
                END";

            try {
                Address addressOut = Connection.Query<Address>(
                    sql: query,
                    param: address,
                    transaction: Transaction).FirstOrDefault();

                return addressOut;
            }
            catch (SqlException)
            {
                Transaction.Rollback();
                throw;
            }
        }

        public void Delete(int addressId)
        {
            string query = @"
                UPDATE [Person]
                SET [AddressId] = NULL
                WHERE [AddressId] = @AddressId

                DELETE FROM [Address]
                WHERE [Id] = @AddressId";

            try
            {
                Connection.Query(
                    sql: query,
                    param: new { AddressId = addressId },
                    transaction: Transaction);

            }
            catch (SqlException)
            {
                Transaction.Rollback();
                throw;
            }
        }
    }
}