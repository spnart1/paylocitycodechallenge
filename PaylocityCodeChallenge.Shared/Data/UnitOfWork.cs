﻿using PaylocityCodeChallenge.Shared.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection connection;
        private IDbTransaction transaction;

        private IDependentRepository dependentRepository;
        private IEmployeeRepository employeeRepository;
        private IAddressRepository addressRepository;

        private bool transactionOpen = false;

        public UnitOfWork(string connectionString)
        {
            this.connection = new SqlConnection(connectionString);
        }

        public IDependentRepository DependentRepository
        {
            get
            {
                if(dependentRepository == null)
                {
                    DependentRepository dependentRepository = new DependentRepository();
                    dependentRepository.setTransaction(transaction);
                    this.dependentRepository = dependentRepository;
                }
                return dependentRepository;
            }
        }

        public IEmployeeRepository EmployeeRepository
        {
            get
            {
                if (employeeRepository == null)
                {
                    EmployeeRepository employeeRepository = new EmployeeRepository();
                    employeeRepository.setTransaction(transaction);
                    this.employeeRepository = employeeRepository;
                }
                return employeeRepository;
            }
        }

        public IAddressRepository AddressRepository
        {
            get
            {
                if (addressRepository == null)
                {
                    AddressRepository addressRepository = new AddressRepository();
                    addressRepository.setTransaction(transaction);
                    this.addressRepository = addressRepository;
                }
                return addressRepository;
            }
        }

        public void BeginNewTransaction()
        {
            if (transactionOpen)
            {
                Dispose();
            }
            this.connection.Open();
            transaction = connection.BeginTransaction();
            transactionOpen = true;
            ResetRepositories();
        }

        public void Commit()
        {
            try
            {
                if (transactionOpen)
                {
                    transaction.Commit();
                    transactionOpen = false;
                }
            }
            catch
            {
                transaction.Rollback();
                transactionOpen = false;
                throw;
            }
            finally
            {
                if (transactionOpen)
                {
                    transaction.Dispose();
                    transactionOpen = false;
                }
            }
        }

        private void ResetRepositories()
        {
            dependentRepository = null;
            employeeRepository = null;
        }

        public void Dispose()
        {
            transaction.Dispose();
            transactionOpen = false;
        }

        ~UnitOfWork()
        {
            if (transactionOpen)
            {
                Dispose();
            }
        }
    }
}
