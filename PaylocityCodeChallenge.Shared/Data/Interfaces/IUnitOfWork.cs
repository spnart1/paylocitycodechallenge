﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IDependentRepository DependentRepository { get; }
        IEmployeeRepository EmployeeRepository { get; }
        IAddressRepository AddressRepository { get; }
        void BeginNewTransaction();
        void Commit();
    }
}
