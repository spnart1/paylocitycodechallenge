﻿using PaylocityCodeChallenge.Shared.Entities;
using System.Collections.Generic;

namespace PaylocityCodeChallenge.Shared.Data.Interfaces
{
    public interface IEmployeeRepository
    {
        Employee Get(int employeeId);

        IEnumerable<Employee> GetAll();

        Employee Add(Employee employee);

        Employee Update(int employeeId, Employee employee);

        bool Delete(int employeeId);
    }
}