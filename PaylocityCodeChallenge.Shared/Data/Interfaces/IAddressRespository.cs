﻿using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data.Interfaces
{
    public interface IAddressRepository
    {
        Address Get(int personId);

        Address Add(int personId, Address address);

        Address Update(int addressId, Address address);

        void Delete(int addressId);
    }
}
