﻿using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Data.Interfaces
{
    public interface IDependentRepository
    {
        Dependent Get(int dependentId);

        Dependent Add(int employeeId, Dependent dependent);

        Dependent Update(int dependentId, Dependent dependent);

        void Delete(int dependentId);
    }
}
