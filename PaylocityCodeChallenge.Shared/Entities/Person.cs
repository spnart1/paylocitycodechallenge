﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Entities
{
    public class Person
    {
        [Required]
        public int PersonId { get; set; }

        public String SSN { get; set; }

        public String FirstName { get; set; }

        public String MiddleName { get; set; }

        public String LastName { get; set; }

        public int AddressId { get; set; }

        public Address Address { get; set; }

        [DataType(DataType.Currency)]
        public decimal? CostPerYearBeforeDiscount { get; set; }

        [DataType(DataType.Currency)]
        public decimal? PersonalYearlyDiscount { get; set; }

        [DataType(DataType.Currency)]
        public decimal? CostPerYearAfterDiscount { get; set; }
    }
}