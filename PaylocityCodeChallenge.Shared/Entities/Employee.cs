﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Entities
{
    public class Employee : Person
    {
        [Required]
        public int EmployeeId { get ; set; }

        public List<Dependent> Dependents { get; set; }

        public int DependentCount { get; set; }

        public int NumberOfDiscountsIncludingDependents { get; set; }

        [DataType(DataType.Currency)]
        public decimal? TotalYearlyCostsIncludingDependents { get; set; }

        [DataType(DataType.Currency)]
        public decimal? NetBiWeeklyPay { get; set; }

        [DataType(DataType.Currency)]
        public decimal? GrossBiWeeklyPay { get; set; }
    }
}