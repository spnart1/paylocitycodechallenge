﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Entities
{
    public class PayData
    {

        [DataType(DataType.Currency)]
        public decimal GrandTotalNetCosts { get; set; }

        [DataType(DataType.Currency)]
        public decimal GrandTotalGrossCosts { get; set; }

        [DataType(DataType.Currency)]
        public decimal GrandTotalDiscountCount { get; set; }

        [DataType(DataType.Currency)]
        public decimal GrandTotalDiscounts { get; set; }

        [DataType(DataType.Currency)]
        public decimal GrandTotalYearlyNetPay { get; set; }

        [DataType(DataType.Currency)]
        public decimal GrandTotalYearlyGrossPay { get; set; }
    }
}
