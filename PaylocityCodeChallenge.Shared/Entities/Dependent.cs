﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Entities
{
    public class Dependent : Person
    {
        public int DependentId { get; set; }
    }
}