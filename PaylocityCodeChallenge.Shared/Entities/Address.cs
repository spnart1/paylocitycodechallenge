﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaylocityCodeChallenge.Shared.Entities
{
    public class Address
    {
        [Required]
        public int AddressId { get; set; }

        public String Street1 { get; set; }

        public String Street2 { get; set; }

        public String City { get; set; }

        public String State { get; set; }

        public String Zip { get; set; }
    }
}
