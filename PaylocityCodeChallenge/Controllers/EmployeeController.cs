﻿using PaylocityCodeChallenge.BusinessLogic;
using PaylocityCodeChallenge.Shared.Data;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaylocityCodeChallenge.Controllers
{
    public class EmployeeController : ApiController
    {
        IUnitOfWork unitOfWork = null;

        public EmployeeController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/<controller>
        public IHttpActionResult Get()
        {
            this.unitOfWork.BeginNewTransaction();
            IEnumerable<Employee> employees = this.unitOfWork.EmployeeRepository.GetAll();
            this.unitOfWork.Commit();

            foreach (var employee in employees)
            {
                PayCalc.AppendPayCalculations(employee);
            }

            return Ok(new {
                employees,
                payData = PayCalc.getPayData(employees)
            });
        }

        // GET api/<controller>/5
        public Employee Get(int id)
        {
            this.unitOfWork.BeginNewTransaction();
            Employee employee = this.unitOfWork.EmployeeRepository.Get(id);
            this.unitOfWork.Commit();
            if (employee != null)
            {
                PayCalc.AppendPayCalculations(employee);
            }

            return employee;
        }

        // POST api/<controller>
        public Employee Post(Employee employee)
        {
            this.unitOfWork.BeginNewTransaction();
            Employee employeeOut = this.unitOfWork.EmployeeRepository.Add(employee);
            this.unitOfWork.Commit();

            if (employeeOut != null)
            {
                PayCalc.AppendPayCalculations(employeeOut);
            }

            return employeeOut;
        }

        // PUT api/<controller>/5
        public Employee Put(int id, Employee employee)
        {
            this.unitOfWork.BeginNewTransaction();
            Employee employeeOut = this.unitOfWork.EmployeeRepository.Update(id, employee);
            this.unitOfWork.Commit();

            if (employeeOut != null)
            {
                PayCalc.AppendPayCalculations(employeeOut);
            }

            return employeeOut;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            this.unitOfWork.BeginNewTransaction();
            this.unitOfWork.EmployeeRepository.Delete(id);
            this.unitOfWork.Commit();
        }
    }
}