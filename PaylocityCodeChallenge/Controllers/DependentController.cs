﻿using PaylocityCodeChallenge.BusinessLogic;
using PaylocityCodeChallenge.Shared.Data;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaylocityCodeChallenge.Controllers
{
    public class DependentController : ApiController
    {
        IUnitOfWork unitOfWork = null;

        public DependentController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/<controller>/5
        public Dependent Get(int id)
        {
            unitOfWork.BeginNewTransaction();
            Dependent dependent = unitOfWork.DependentRepository.Get(id);
            unitOfWork.Commit();

            if (dependent != null)
            {
                PayCalc.AppendPayCalculations(dependent);
            }

            return dependent;
        }

        // POST api/<controller>
        public Dependent Post(int id, Dependent dependent)
        {
            unitOfWork.BeginNewTransaction();
            Dependent dependentOut = unitOfWork.DependentRepository.Add(id, dependent);
            unitOfWork.Commit();

            if (dependentOut != null)
            {
                PayCalc.AppendPayCalculations(dependentOut);
            }

            return dependentOut;
        }

        // PUT api/<controller>/5
        public Dependent Put(int id, Dependent dependent)
        {
            unitOfWork.BeginNewTransaction();
            Dependent dependentOut = unitOfWork.DependentRepository.Update(id, dependent);
            unitOfWork.Commit();

            if (dependentOut != null)
            {
                PayCalc.AppendPayCalculations(dependentOut);
            }

            return dependentOut;
        }

        // DELETE api/<controller>/5
        public IHttpActionResult Delete(int id)
        {
            unitOfWork.BeginNewTransaction();
            unitOfWork.DependentRepository.Delete(id);
            unitOfWork.Commit();

            return Ok();
        }
    }
}