﻿using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaylocityCodeChallenge.Controllers
{
    public class AddressController : ApiController
    {
        IUnitOfWork unitOfWork = null;

        public AddressController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/<controller>/5
        public Address Get(int id)
        {
            this.unitOfWork.BeginNewTransaction();
            Address address = this.unitOfWork.AddressRepository.Get(id);
            this.unitOfWork.Commit();
            return address;
        }

        // POST api/<controller>
        public Address Post(int id, Address address)
        {
            this.unitOfWork.BeginNewTransaction();
            Address addressOut = this.unitOfWork.AddressRepository.Add(id, address);
            this.unitOfWork.Commit();
            return addressOut;
        }

        // PUT api/<controller>/5
        public Address Put(int id, Address address)
        {
            this.unitOfWork.BeginNewTransaction();
            Address addressOut = this.unitOfWork.AddressRepository.Update(id, address);
            this.unitOfWork.Commit();
            return addressOut;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            this.unitOfWork.BeginNewTransaction();
            this.unitOfWork.AddressRepository.Delete(id);
            this.unitOfWork.Commit();
        }
    }
}