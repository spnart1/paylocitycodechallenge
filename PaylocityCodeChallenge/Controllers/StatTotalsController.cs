﻿using PaylocityCodeChallenge.BusinessLogic;
using PaylocityCodeChallenge.Shared.Data.Interfaces;
using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PaylocityCodeChallenge.Controllers
{
    public class StatTotalsController : ApiController
    {
        IUnitOfWork unitOfWork = null;

        public StatTotalsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET api/<controller>/5
        public IHttpActionResult Get()
        {
            this.unitOfWork.BeginNewTransaction();
            IEnumerable<Employee> employees = this.unitOfWork.EmployeeRepository.GetAll();
            this.unitOfWork.Commit();

            foreach (var employee in employees) {
                PayCalc.AppendPayCalculations(employee);
            }

            return Ok();
        }
    }
}