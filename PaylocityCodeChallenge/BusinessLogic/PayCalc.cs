﻿using PaylocityCodeChallenge.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace PaylocityCodeChallenge.BusinessLogic
{
    public static class PayCalc
    {
        public const int EMPLOYEE_COST_PER_YEAR = 1000;
        public const int DEPENDENT_COST_PER_YEAR = 500;
        public const decimal DISCOUNT_RATE = 0.10m;

        public static Person AppendPayCalculations(Person person)
        {
            Boolean getsDiscount = person.FirstName.StartsWith("A", true, CultureInfo.CurrentCulture);

            if (person is Employee)
            {
                Employee employee = (Employee)person;

                if (getsDiscount)
                {
                    employee.NumberOfDiscountsIncludingDependents++;
                }

                if (employee.Dependents != null)
                {
                    foreach (Dependent dependent in employee.Dependents)
                    {
                        employee.DependentCount++;
                        AppendPayCalculations(dependent);
                        if (dependent.PersonalYearlyDiscount != null && dependent.PersonalYearlyDiscount > 0)
                        {
                            employee.NumberOfDiscountsIncludingDependents++;
                        }
                    }

                }
                person.CostPerYearBeforeDiscount = EMPLOYEE_COST_PER_YEAR;
            }
            else if (person is Dependent)
            {
                person.CostPerYearBeforeDiscount = DEPENDENT_COST_PER_YEAR;
            }

            person.PersonalYearlyDiscount = person.FirstName.StartsWith("A", true, CultureInfo.CurrentCulture)   ? person.CostPerYearBeforeDiscount * DISCOUNT_RATE : 0.0m;

            person.CostPerYearAfterDiscount = person.CostPerYearBeforeDiscount - person.PersonalYearlyDiscount;

            if (person is Employee)
            {
                Employee employee = (Employee)person;

                employee.TotalYearlyCostsIncludingDependents = employee.CostPerYearAfterDiscount ?? 0.0m;
                if (employee.Dependents != null) {
                    foreach (Dependent dependent in employee.Dependents)
                    {
                        employee.TotalYearlyCostsIncludingDependents += dependent.CostPerYearAfterDiscount;
                    }
                }
                employee.GrossBiWeeklyPay = 2000.0m;
                employee.NetBiWeeklyPay = 2000.0m - (employee.TotalYearlyCostsIncludingDependents / 26.0m);
            }

            return person;
        }

        /*
        public int GrandTotalNetCosts { get; set; } after discounts

        public int GrandTotalGrossCosts { get; set; }

        public int GrandTotalDiscountCount { get; set; }

        public int GrandTotalDiscounts { get; set; }
        */

        public static PayData getPayData(IEnumerable<Employee> employees)
        {
            if (employees == null)
            {
                return null;
            }

            PayData payData = new PayData();

            foreach (var employee in employees)
            {
                payData.GrandTotalNetCosts += employee.CostPerYearAfterDiscount ?? 0.0m;
                payData.GrandTotalGrossCosts += employee.CostPerYearBeforeDiscount ?? 0.0m;
                payData.GrandTotalDiscountCount += employee.NumberOfDiscountsIncludingDependents;
                payData.GrandTotalDiscounts += employee.PersonalYearlyDiscount ?? 0.0m;

                if (employee.Dependents != null)
                {
                    foreach (var dependent in employee.Dependents)
                    {
                        payData.GrandTotalNetCosts += employee.CostPerYearAfterDiscount ?? 0.0m;
                        payData.GrandTotalGrossCosts += employee.CostPerYearBeforeDiscount ?? 0.0m;
                        payData.GrandTotalDiscountCount += employee.NumberOfDiscountsIncludingDependents;
                        payData.GrandTotalDiscounts += employee.PersonalYearlyDiscount ?? 0.0m;
                    }
                }
            }

            payData.GrandTotalYearlyGrossPay = 2000 * 26 * employees.Count();
            payData.GrandTotalYearlyNetPay = payData.GrandTotalYearlyGrossPay - payData.GrandTotalNetCosts;

            return payData;
        }
    }
}