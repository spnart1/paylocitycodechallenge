[assembly: WebActivator.PostApplicationStartMethod(typeof(PaylocityCodeChallenge.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace PaylocityCodeChallenge.App_Start
{
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.Http;
    using PaylocityCodeChallenge.Shared.Data;
    using PaylocityCodeChallenge.Shared.Data.Interfaces;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;


    public static class SimpleInjectorWebApiInitializer
    {
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            container.Register<IUnitOfWork>(() => new UnitOfWork(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString), Lifestyle.Scoped);
            container.Register<IEmployeeRepository>(() => new EmployeeRepository(), Lifestyle.Scoped);
            container.Register<IDependentRepository>(() => new DependentRepository(), Lifestyle.Scoped);
            container.Register<IAddressRepository>(() => new AddressRepository(), Lifestyle.Scoped);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}