You�ll need:

	Node.js
	SQL Server
	Visual Studio


PaylocityCodeChallenge.UI is a node project folder.  To transpile a new bundle.js into the Content folder of the main solution project(PaylocityCodeChallenge), cd to PaylocityCodeChallenge.UI and run the following commands:

	npm install
	npm run compile

You can run CreateDB.sql located in the root folder to generate the db used in the solution.  The web.config�s �default� connection string will probably need to be editted to match the instance of SQL Server the db is installed on
